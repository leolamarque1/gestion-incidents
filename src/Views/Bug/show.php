<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include("header.php")?>
</head>
<body>
<?php include("nav.php")?>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <a class="alignements" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons">chevron_left</i> Retour à la liste </a>
      <h2 class="header" >Fiche descriptive d'incident<i class="material-icons">account_circle</i></h2><br><br>
 
    </div>
  </div>


  <div class="container">
    <div class="row">
        <form class="col s12">
          <div class="row">
            <div class="input-field col s6">
              <p>
                  Nom de l'incidents :&emsp; <?=$bug->getTitle();?>
              </p>
            </div>
            <div class="input-field col s6">
                <p>
                    Date d'observation : <?php echo $bug->getCreatedAt()->format("d/m/Y");?>
                </p>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
                <p>
                    Description de l'incident :&emsp; <?=$bug->getDescription();?>
                </p>
              </div>
          </div>
        </form>
      </div>
      
  </div><br>

  <?php include("footer.php")?>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>

  </body>
</html>
