<!DOCTYPE html>
<html lang="en">
<head>
<?php include("header.php")?>
</head>
<body>
<?php include("nav.php")?>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <a class="alignements" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons">chevron_left</i> Retour à la liste </a>
      <h2 class="header" >Rapport d'incident<i class="material-icons">account_circle</i></h2><br><br>
 
    </div>
  </div>

  <div class="container">
    <div class="row">
        <form class="col s12" method='post'>
          <div class="row">
            <div class="input-field col s6">
              <input placeholder="Placeholder" id="first_name" type="text" class="validate" name="title">
              <label class="active" for="first_name" >Nom de l'incident</label>
            </div>
            <div class="input-field col s6">
              <input type="date" class="validate" name="date">
              <label class="active" for="last_name" >Date</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
                <textarea placeholder="Type here" id="textarea1" class="materialize-textarea" name="description"></textarea>
                <label class="active" for="textarea1" >Description de l'incident</label>
              </div>
          </div>
          <button class="btn waves-effect waves-light" type="submit" name="submit">Ajouter
            <i class="material-icons right">send</i>
        </button>
        </form>

</div>

</div><br>

  <?php include("footer.php")?>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/materialize.js"></script>
  <script src="/js/init.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems);
    });
  </script>

  </body>
</html>
