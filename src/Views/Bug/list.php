<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include("header.php")?>
</head>
<body>
<?php include("nav.php")?>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h2 class="header" >Liste des incidents<i class="material-icons">account_circle</i></h2>
      <a class="alignements" href="<?php echo PUBLIC_PATH; ?>bug/add"><i class="material-icons">add_circle</i> Rapporter un incidents </a><br><br><br>
 
    </div>
  </div>


  <div class="container">
    <table class="striped">
      <thead>
        <tr class="grey lighten-1">
            <th>Id</th>
            <th>Sujet</th>
            <th>Date</th>
            <th>Clôture</th>
            <th></th>
        </tr>
      </thead>

      <tbody>
      <?php 
        foreach ($bugs as $bug) {
            echo '
                <tr>
                    <td>'. $bug->getId() .'</td>
                    <td>'. $bug->getTitle() .'</td>
                    <td>'. $bug->getCreatedAt()->format("d/m/Y") .'</td>';
                    if($bug->getClosedAt() != null) {
                        echo '<td>'.$bug->getClosedAt()->format("d/m/Y").'</td>';
                    } else {
                        echo '<td></td>';
                    }
                    echo '
                        <td><a href="'.PUBLIC_PATH.'bug/show/'.$bug->getId().'">Afficher</a></td>
                        </tr>
                    ';
              };
        ?>
      </tbody>
    </table>
  </div><br>

  <?php include("footer.php")?>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
