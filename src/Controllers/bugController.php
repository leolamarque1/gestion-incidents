<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        // $bugs = [];
        $manager = new BugManager();

        $bugs = $manager->findAll();
        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

public function add()
{

        
        $manager = new BugManager();
        // Ajout d'un incident
        
        if (isset($_POST['submit'])){    
        $bug = new Bug();
        //On récupère les valeurs entrées par l'utilisateur :
        $bug->setTitle($_POST['title']);
        $bug->setDescription($_POST['description']);
        $bug->setCreatedAt($_POST['date'].' 00:00:00'); 

        $manager->add($bug);

        header('Location: ' . PUBLIC_PATH . "bug");
       
    }else{

        $content = $this->render('src/Views/Bug/add', []);

        return $this->sendHttpResponse($content, 200);

    }
    }
}
